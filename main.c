#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include "qrcodegen.h"

#define MAX_INPUT_BYTES 2953

// --- SVG stuff ---
static const char xml_head[] = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n";
static const char svg_tail[] = "</svg>\n";
static const char doctype[] = "<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\" \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">\n";
static const char tag_svg[] =
        "<svg shape-rendering=\"crispEdges\" width=\"400\" height=\"400\" viewBox=\"-5 -5 %d %d\" xmlns=\"http://www.w3.org/2000/svg\" >\n"
        "\t<rect x=\"-5\" y=\"-5\" width=\"100%%\" height=\"100%%\" fill=\"white\" />\n"
        ;
static const char tag_element[] = "\t<rect x=\"%d\" y=\"%d\" width=\"%d.0\" height=\"1.0\" />\n";
static const char error_svg[] = "<!-- Error -->\n";
// --- QR buffers & stuff ---
static uint8_t in_buffer[qrcodegen_BUFFER_LEN_MAX];
static uint8_t qr_buffer[qrcodegen_BUFFER_LEN_MAX];
static char    *out_buffer = NULL;
static size_t  out_buffer_index = 0;
static size_t  in_count = 0;
// --- Constants ---
#define MAX_INT_DIGITS 20

static void read_data(char* str)
{
    strncpy((char*)in_buffer, str, sizeof(in_buffer));
    in_count = strlen((char*)in_buffer);
    fprintf(stdout, "Contents: %s\n", in_buffer);
}

static void generate()
{
    bool ok = qrcodegen_encodeBinary(
        in_buffer, in_count, qr_buffer,
        qrcodegen_Ecc_LOW, qrcodegen_VERSION_MIN, qrcodegen_VERSION_MAX,
        qrcodegen_Mask_AUTO, true);

    //Format buffer, big enough to hold the string + 2 integers
    if(ok)
    {
        size_t str_index = 0;
        int size = qrcodegen_getSize(qr_buffer);
        size_t out_buffer_size = 0;
        //Calculate out_buffer size
        {
            out_buffer_size += sizeof(xml_head);
            out_buffer_size += sizeof(doctype);
            out_buffer_size += (sizeof(tag_svg) + (MAX_INT_DIGITS * 2));
            out_buffer_size += (sizeof(tag_element) + (MAX_INT_DIGITS * 2)) * size * size;
            out_buffer_size += sizeof(svg_tail);
        }
        //If buffer had anything, free() it
        if(out_buffer)
        {
            free(out_buffer);
            out_buffer = NULL;
        }
        //Allocate buffer
        {
            out_buffer = (char*)malloc(out_buffer_size + 1);
            if(out_buffer == NULL)
            {
                puts("[ERR] Unable to allocate out buffer");
                return;
            }
        }
        out_buffer_index = 0;
#ifndef __EMSCRIPTEN__
        //<?xml>
        out_buffer_index += snprintf(out_buffer + out_buffer_index, out_buffer_size - out_buffer_index, xml_head);
        //<!Doctype>
        out_buffer_index += snprintf(out_buffer + out_buffer_index, out_buffer_size - out_buffer_index, doctype);
#endif
        //<svg>
        out_buffer_index += snprintf(out_buffer + out_buffer_index, out_buffer_size - out_buffer_index, tag_svg, size + 10, size + 10);
        for(int y = 0; y < size; y++)
        {
            int run_length = 0;
            for(int x = 0; x < size; x++)
            {
                if(qrcodegen_getModule(qr_buffer, x, y))
                {
                    run_length++;
                    int print_tag = 0;

                    //Print a tag if...
                    //...this is the last column.
                    if (x == (size-1))
                    {
                        print_tag = 1;
                    }
                    //...the next column is blank.
                    else if (! qrcodegen_getModule(qr_buffer, x+1, y))
                    {
                        print_tag = 1;
                    }

                    //<rect />
                    if(print_tag)
                    {
                        assert(out_buffer_index < out_buffer_size);
                        int run_x = x - (run_length - 1);
                        out_buffer_index += snprintf(out_buffer + out_buffer_index, out_buffer_size - out_buffer_index, tag_element, run_x, y, run_length);
                        run_length = 0;
                    }
                }
            }
        }
        //</svg>
        out_buffer_index += snprintf(out_buffer + out_buffer_index, out_buffer_size - out_buffer_index, svg_tail);
    }
    else
    {
        puts("[ERR] QRencode failed");
        out_buffer = (char*)malloc(sizeof(error_svg));
        if(out_buffer)
        {
            memcpy(out_buffer, error_svg, sizeof(error_svg));
        }
        else
        {
            puts("[ERR] Unable to allocate out buffer");
        }
    }
}

/*
 * This program converts argument 1 into a QR code.
 * UTF-8 input is presumed (no null-values allowed)
 */
int main(int argc, char** argv)
{
#ifdef __EMSCRIPTEN__
    puts("Webassembly program loaded successfully");
#else
    fprintf(stdout, "qrcode-svg started\n");
    if(argc > 2)
    {
        read_data(argv[1]);
    }
    else
    {
        read_data("Please add some data");
    }
    generate();
    if(out_buffer)
    {
        puts(out_buffer);
    }
    fprintf(stdout, "qrcode-svg end\n");
#endif
    return 0;
}

#ifdef __EMSCRIPTEN__
void* em_qrcode_svg(char* str)
{
    read_data(str);
    generate();
    return out_buffer;
}
#endif
